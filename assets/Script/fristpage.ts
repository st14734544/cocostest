// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;
const {} = cc._decorator
@ccclass
export default class fristpage extends cc.Component {

    @property(cc.EditBox)
    // const checktext = /^[a-zA-Z0-9\s]*$/
    // editbox: cc.EditBox
    @property
    text: string = ''
    player: {
        default: null,
        type: cc.Node
    }
    onLoad () {
        // add key down and key up event
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        // cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
    }
    onKeyDown (event) {
        
        const checktext = /^[a-zA-Z0-9\s]*$/
        console.log(checktext.test(event));
        if (checktext.test(event)==true){
            console.log(cc.EditBox);
        }
        // console.log(cc.EditBox);
    }
    start(){
       const label = this.getComponent(cc.Label)
        label.string = this.text;
        var node = this.node;
        node.x = 100;

        var label1 = this.getComponent(cc.Label);
        var text = this.name + 'started';

        if (label) {
            label.string = "Hello";
        }
        else {
            cc.error("Something wrong?");
        }

        var playerComp = this.player;
    }
    // callback(editbox) {
    //     // this.text = editbox
    //     const checktext = /^[a-zA-Z0-9\s]*$/
    //     if (checktext.test(editbox)){
    //         this.text = editbox
    //         console.log('editbox'+ this.text)
    //     }
    //     // The parameter of the callback is the editbox component.
    //     // do whatever you want with the editbox.
    // }
    addComponent(nextPage){
        this.node.on(cc.Node.EventType.TOUCH_START, function (event) {
            cc.director.loadScene("play")
        });
    }

}